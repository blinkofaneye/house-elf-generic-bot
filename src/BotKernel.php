<?php
/**
 * Bot kernel
 */

namespace HouseElf;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\ArrayCache;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Interfaces\MiddlewareInterface;
use BotMan\BotMan\Interfaces\CacheInterface;

use Symfony\Component\HttpFoundation\Request;

use Illuminate\Database\Capsule\Manager as Capsule;

use HouseElf\Core\BotConfig;
use HouseElf\Core\ServiceConfig;
use HouseElf\Features\AbstractFeature;
use HouseElf\Services\ServiceInterface;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class BotKernel
{
    const MSG_AUTH_NEEDED = 'Vous devez vous authentifier!';

    private $bot_config;
    private $botman;
    private $NLP;
    private $bot_mock;

   /**
    * @param array $config
    * @param MiddlewareInterface $NLPMiddleware
    * @param CacheInterface $cache
    * @param Request $request
    * @param BotMan $bot_mock
    */
    public function __construct(
        array $config,
        MiddlewareInterface $NLPMiddleware,
        CacheInterface $cache = null,
        BotMan $bot_mock = null
    ) {
        // Bootstrap the kernel
        $this->bootstrap($config, $cache);

        // Natural Language Processing
        $this->NLP = $NLPMiddleware;

        // For mocking purposes
        $this->bot_mock = $bot_mock;
    }

   /**
    * Deactivates a feature
    *
    * @param string $feature
    */
    public function deactivateFeature(string $feature)
    {
        if (isset($this->bot_config['features'][$feature])) {
            $this->bot_config['features'][$feature] = false;
        }
    }

   /**
    * Make the bot listen
    */
    public function listen()
    {
        $this->NLP->listenForAction();
        $this->botman->middleware->received($this->NLP);

        // match features
        foreach ($this->bot_config['features'] as $feature => $activated) {
            if ($activated) {
                $this->registerFeature($feature);
            }
        };

        // match services
        if (isset($this->bot_config['services'])) {
            foreach ($this->bot_config['services'] as $service) {
                $this->registerService($service);
            }
        }

        // set fallback feature
        $this->botman->fallback(function ($bot) {
            $this->executeFallbackFeature($bot);
        });

        $this->botman->listen();
    }

   /**
    *  Instantiate a class then call class->$method(...$arg)
    *
    * @param string $class
    * @param string $extends
    * @param string $method
    * @param mixed $arg
    * @return mixed
    */
    public static function instantiateAndExecute(string $class, string $extends, string $method, ...$arg)
    {
        $obj = new $class();
        if ($obj instanceof $extends) {
            return $obj->{$method}(...$arg);
        } else {
            throw new \InvalidArgumentException("{$class} does not extend {$extends}!");
        }
    }

   /**
    * Registers a feature
    *
    * @param string $feature
    */
    protected function registerFeature(string $feature)
    {
        $this->botman->hears($feature, function (BotMan $bot) use ($feature) {
            if ($this->bot_mock !== null) {
                $bot = $this->bot_mock;
            }
            $this->executeFeature($feature, $bot);
        })->middleware($this->NLP);
    }

   /**
    * Registers a service
    *
    * @param string $service
    */
    protected function registerService(string $service)
    {
        $this->botman->hears($service, function (BotMan $bot) use ($service) {
            if ($this->bot_mock !== null) {
                $bot = $this->bot_mock;
            }
            // service handling
            $name = $this->serviceNameFromAction($service);
            $config = $this->getServiceConfig($name);
            // check if need authentication
            if ($config['need_authentication']) {
                // check if user is already authenticated
                $storage = $bot->userStorage();
                $flag = $storage->get('authenticated');
                if ($flag) {
                    $this->executeService($service, $bot);
                } else {
                    $bot->reply(self::MSG_AUTH_NEEDED);
                }
            } else {
                $this->executeService($service, $bot);
            }
        })->middleware($this->NLP);
    }

   /**
    * Executes feature code
    *
    * @param string $feature
    * @param BotMan $bot
    */
    protected function executeFeature(string $feature, BotMan $bot)
    {
        $class = '\\HouseElf\\Features\\'.ucfirst(explode('_', $feature)[0]);
        $this->instantiateAndExecute(
            $class,
            AbstractFeature::class,
            'setup',
            $this->bot_config
        )->main($bot);
    }

   /**
    * Executes service code
    *
    * @param string $service
    * @param BotMan $bot
    */
    protected function executeService(string $service, BotMan $bot)
    {
        // load service configs
        $name = $this->serviceNameFromAction($service);
        $config = $this->getServiceConfig($name);
        $class = '\\'.ucfirst($this->bot_config['name']).
                 '\\Services\\'.$name.'\\'.$config['service_class'];

        $this->instantiateAndExecute($class, ServiceInterface::class, 'main', $bot);
    }

   /**
    * Calls the right fallback code
    *
    * @param BotMan $bot
    */
    protected function executeFallbackFeature(BotMan $bot)
    {
        if ($this->bot_mock !== null) {
            $bot = $this->bot_mock;
        }

        if ($this->bot_config['implements_fallback_feature']) {
            $class = '\\'.ucfirst($this->bot_config['name']).'\\Features\\Fallback';
            $this->instantiateAndExecute($class, AbstractFeature::class, 'main', $bot);
        } else {
            $this->executeFeature('fallback_feature', $bot);
        }
    }

   /**
    * Get service config
    *
    * @param strig $service_name
    * @return array
    */
    protected function getServiceConfig(string $service_name)
    {
        $path = $this->bot_config['path'].'/services/'.$service_name;
        return (new ServiceConfig(['path' => $path]))->getConfig();
    }

   /**
    * Get service name from action
    *
    * @param string $action
    * @return string
    */
    protected function serviceNameFromAction(string $action)
    {
        $name = ucfirst(explode('_', $action)[0]);
        return $name;
    }

    /**
     * Loads the bot configuration.
     *
     * @param array $config
     * @return void
     */
    protected function loadConfig(array $config)
    {
        $this->bot_config = (new BotConfig($config))->getConfig();
    }

    /**
     * Loads the bot driver.
     *
     * @return void
     */
    protected function loadDriver()
    {
        if (class_exists($this->bot_config['driver'])) {
            DriverManager::loadDriver($this->bot_config['driver']);
        } else {
            throw new \InvalidArgumentException("Unknown driver {$this->bot_config['driver']}");
        }
    }

    /**
     * Bootstraps the kernel.
     *
     * @param array $config
     * @param CacheInterface $cache
     * @return void
     */
    protected function bootstrap(array $config, $cache)
    {
        $this->loadConfig($config);

        // load bot dependencies
        if (isset($this->bot_config['path'])) {
            @include_once $this->bot_config['path'].'/vendor/autoload.php';
        }

        // load facebook driver
        $this->loadDriver();

        // set up database
        if (isset($this->bot_config['database'])) {
            self::setUpDatabase($this->bot_config['database']);
        }

        // facebook webhook config
        if (isset($this->bot_config['facebook'])) {
            $fb_config['facebook'] = $this->bot_config['facebook'];
        } else {
            $fb_config['facebook'] = [];
        }

        // this is it!
        $this->botman = BotManFactory::create(
            $fb_config,
            $cache
        );
    }

    /**
     * Sets up the database.
     *
     * @param array $dbconfig The database configuration.
     * @return void
     */
    public static function setUpDatabase(array $dbconfig)
    {
        $capsule = new Capsule;
        $capsule->addConnection($dbconfig);
        $capsule->setAsGlobal();
    }
}
