<?php

namespace HouseElf\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

use HouseElf\BotKernel;
use HouseElf\Migrations\Botusers;

class Migrate extends Command
{
    protected static $defaultName = 'migrate';

    /**
     * Define command arguments
     */
    protected function configure()
    {
        $this->setDescription('Run database migrations');
        $this->setHelp('Creates the database scheme for you');

        $this->addArgument(
            'dbconfig',
            InputArgument::REQUIRED,
            'The database yaml config file'
        );
    }

    /**
     * Command code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // load config file
        $config = Yaml::parseFile($input->getArgument('dbconfig'));
        // setup database
        BotKernel::setUpDatabase($config);
        // build table
        $botusers = new Botusers;
        $botusers->down();
        $botusers->up();
    }
}
