<?php
/**
 * Service interface
 */

namespace HouseElf\Services;

use BotMan\BotMan\BotMan;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
interface ServiceInterface
{
    public function main(BotMan $bot);
}
