<?php
/**
 * Greetings feature
 */

namespace HouseElf\Features;

use BotMan\BotMan\BotMan;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class Greetings extends AbstractFeature
{

    public function main(BotMan $bot)
    {
        $extras = $bot->getMessage()->getExtras();
        $bot->reply($extras['apiReply']);
    }
}
