<?php
/**
 * Feature interface
 */

namespace HouseElf\Features;

use BotMan\BotMan\BotMan;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class Fallback extends AbstractFeature
{
    public function main(BotMan $bot)
    {
        $bot->reply('Fallback!');
    }
}
