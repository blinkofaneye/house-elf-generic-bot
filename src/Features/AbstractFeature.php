<?php
/**
 * Features base class
 */

namespace HouseElf\Features;

use BotMan\BotMan\BotMan;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
abstract class AbstractFeature
{
    private $config;

    public function setup($config): AbstractFeature
    {
        $this->config = $config;
        return $this;
    }

    public function getConfig()
    {
        return $this->config;
    }

    abstract public function main(BotMan $bot);
}
