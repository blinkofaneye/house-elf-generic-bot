<?php
/**
 * Authentication feature
 */

namespace HouseElf\Features;

use Illuminate\Database\Capsule\Manager as Capsule;

use BotMan\BotMan\BotMan;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class Authentication extends AbstractFeature
{
    const MSG_EMPTY_INFO   = 'Veuillez renseigner vos informations de connexion';
    const MSG_EMPTY_LOGIN  = 'Plus que votre identifiant et vous serez authentifié!';
    const MSG_EMPTY_PASSWD = 'Plus que votre mot de passe et vous serez authentifié!';
    const MSG_AUTH_SUCCESS = 'Authentification réussie!';
    const MSG_AUTH_FAILURE = "Echec de l'authentification!";

   /**
    * Entry point
    */
    public function main(BotMan $bot)
    {
        $parameters = $this->setupParameters($bot);

        return $this->authenticate($parameters, $bot);
    }

   /**
    * Check and save in botman storage
    *
    * @param BotMan $bot
    * @return array
    */
    private function setupParameters(BotMan $bot)
    {
        // get provided auth infos
        $parameters = $bot->getMessage()->getExtras('apiParameters');
        $parameters['login']    = $parameters['login'] ?? '';
        $parameters['password'] = isset($parameters['password']) &&
                                  !empty($parameters['password']) ?
                                  hash('sha256', $parameters['password']) :
                                  '';

        // get auth infos from storage
        $storage = $bot->userStorage();
        $stored_login = $storage->get('login');
        $stored_password = $storage->get('password');

        // if provided login is empty check if the one in storage is not
        if (empty($parameters['login']) && !empty($stored_login)) {
            $parameters['login'] = $stored_login;
        }
        // if provided passwd is empty check if the one in storage is not
        if (empty($parameters['password']) && !empty($stored_password)) {
            $parameters['password'] = $stored_password;
        }

        // save in storage
        foreach ($parameters as $key => $value) {
            if (!empty($value) && ($value !== ${'stored_'.$key})) {
                $storage->save([$key => $value]);
            }
        }

        return $parameters;
    }

   /**
    * Check and save information in storage and database
    *
    * @param array $parameters
    * @param BotMan $bot
    * @return bool
    */
    private function authenticate(array $parameters, BotMan $bot)
    {
        $login_empty = empty($parameters['login']);
        $password_empty = empty($parameters['password']);

        // possible cases
        if ($login_empty && $password_empty) {
            $bot->reply(self::MSG_EMPTY_INFO);
        } elseif ($login_empty) {
            $bot->reply(self::MSG_EMPTY_LOGIN);
        } elseif ($password_empty) {
            $bot->reply(self::MSG_EMPTY_PASSWD);
        } else {
            $storage = $bot->userStorage();

            $match = (bool) Capsule::table('botusers')->where($parameters)
                                                      ->exists();

            // if there is a user matching
            if ($match) {
                $bot->reply(self::MSG_AUTH_SUCCESS);
                // save user state
                $storage->save(['authenticated' => true]);
                return true;
            } else {
                $bot->reply(self::MSG_AUTH_FAILURE);
            }

            // delete auth infos in storage
            $storage->delete();
        }

        return false;
    }
}
