<?php
/**
 * Help feature
 */

namespace HouseElf\Features;

use BotMan\BotMan\BotMan;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class Help extends AbstractFeature
{
    const MSG_HELP_NOT_FOUND = 'Aide non disponible, désolé...';

   /**
    * Entry point
    */
    public function main(BotMan $bot)
    {
        $config = $this->getConfig();
        if (isset($config['help_text'])) {
            $bot->reply($config['help_text']);
        } else {
            $bot->reply(self::MSG_HELP_NOT_FOUND);
        }
    }
}
