<?php
/**
 * config base class
 */

namespace HouseElf\Core;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
abstract class Config
{
    protected $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        // extend real config
        $this->config += $config;
        // then merge the two configs
        $this->config = $this->mergeConfigs($this->config, $config);

        if (isset($config['path'])) {
            $file_config = Yaml::parseFile($config['path'].'/config.yaml');
            if (!empty($file_config)) {
                $this->config += $file_config;
                $this->config = $this->mergeConfigs($this->config, $file_config);
            }
        }
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Merge two arrays ($config must have the same keys as $override)
     *
     * @param array $config
     * @param array $override
     * @return array
     */
    private function mergeConfigs(array $config, array $override)
    {
        foreach ($override as $key => $value) {
            if (is_array($value)) {
                $config[$key] = $this->mergeConfigs($config[$key], $value);
            } else {
                $config[$key] = $value;
            }
        }

        return $config;
    }
}
