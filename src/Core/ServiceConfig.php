<?php
/**
 * service config class
 */

namespace HouseElf\Core;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class ServiceConfig extends Config
{
    protected $config = ['need_authentication' => false];
}
