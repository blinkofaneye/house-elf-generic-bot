<?php
/**
 * bot config class
 */

namespace HouseElf\Core;

use BotMan\Drivers\Facebook\FacebookDriver;

/**
 * @package generic_bot
 * @category bot
 * @author blinkofaneye
 */
class BotConfig extends Config
{
    protected $config = ['features' => ['authentication_feature' => false,
                                        'greetings_feature'      => true],
                         'implements_fallback_feature' => false,
                         'driver' => FacebookDriver::class];
}
