<?php

namespace HouseElf\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * The botusers table migration.
 * Assumes the database has already been set up.
 */
class Botusers
{
    public function up()
    {
        Capsule::schema()->create('botusers', function ($table) {
            $table->increments('id');
            $table->string('login')->unique();
            $table->string('password');
            $table->timestamps();
        });
    }

    public function down()
    {
        Capsule::schema()->dropIfExists('botusers');
    }
}
