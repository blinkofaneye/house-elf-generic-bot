# House-elf generic bot

## Installation

Add "house-elf/generic_bot" to your composer.json file

## Database

Migrate

```sh
$ php vendor/bin/console migrate <dbconfig.yaml>
```

See dbconfig.example.yaml

## Bot file configuration

Default:

```yaml
name:
features:
    authentication_feature: false
    greetings_feature: true
services: []
implements_fallback_feature: false
driver: BotMan\Drivers\Facebook\FacebookDriver
```

### Available features

* authentication_feature
* greetings_feature
* help_feature

## Service file configuration

Default

```yaml
need_authentication: false
```

### Available fields

* need_authentication
* service_class
