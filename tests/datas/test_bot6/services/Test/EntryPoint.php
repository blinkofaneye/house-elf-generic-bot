<?php

namespace TestBot6\Services\Test;

use BotMan\BotMan\BotMan;

use HouseElf\Services\ServiceInterface;

class EntryPoint implements ServiceInterface
{
    public function main(BotMan $botman)
    {
        $botman->reply('authentication test passed!');
    }
}
