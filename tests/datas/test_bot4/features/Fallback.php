<?php

namespace TestBot4\Features;

use BotMan\BotMan\BotMan;

use HouseElf\Features\AbstractFeature;

class Fallback extends AbstractFeature
{
    public function main(BotMan $bot)
    {
        $bot->reply('fallback feature executed!');
    }
}
