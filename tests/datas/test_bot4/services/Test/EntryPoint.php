<?php

namespace TestBot4\Services\Test;

use BotMan\BotMan\BotMan;

use HouseElf\Services\ServiceInterface;

class EntryPoint implements ServiceInterface
{
    public function main(BotMan $bot)
    {
        $bot->reply('test service executed!');
    }
}
