<?php
/**
 * Tests for the Greetings class
 */

namespace HouseElf\Tests\Features;

use PHPUnit\Framework\TestCase;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;

use HouseElf\Features\Greetings;
use HouseElf\Tests\BotKernelTrait;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Greetings feature
 */
class GreetingsTest extends TestCase
{
    use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testUsesNlpReply()
    {
        $extras = ['apiReply' => 'Test reply'];
        $msg_stub = \Mockery::mock(IncomingMessage::class)->makePartial();
        $msg_stub->shouldReceive('getExtras')
                 ->once()
                 ->andReturns($extras);
        $mock = \Mockery::mock(BotMan::class)->makePartial();
        $mock->shouldReceive('getMessage')
             ->once()
             ->andReturns($msg_stub);
        $mock->shouldReceive('reply')
             ->once()
             ->with($extras['apiReply']);

        $feature = new Greetings();
        $feature->main($mock);
    }
}
