<?php
/**
 * Tests for the Authentication class
 */

namespace HouseElf\Features;

use BotMan\BotMan\BotMan;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 */
class Test extends AbstractFeature
{
    public function main(BotMan $bot)
    {
        $bot->reply('test feature executed!');
    }
}
