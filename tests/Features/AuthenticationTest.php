<?php
/**
 * Tests for the Authentication class
 */

namespace HouseElf\Tests\Features;

use PHPUnit\DbUnit\DataSet\YamlDataSet;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use BotMan\BotMan\Storages\Storage;
use BotMan\BotMan\Storages\Drivers\FileStorage;

use HouseElf\Features\Authentication;
use HouseElf\Tests\GenericDatabaseTestCase;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Authentication feature
 */
class AuthenticationTest extends GenericDatabaseTestCase
{
    use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    private $bot;
    private $storage;
    private $auth;
    private $msg;

    public function setUp(): void
    {
        parent::setUp();

        $this->bot = \Mockery::mock(BotMan::class)->makePartial();
        $file_storage = new FileStorage(__DIR__.'/../datas/botman_storage');
        $this->storage = (new Storage($file_storage))->setPrefix('user_')
                                                     ->setDefaultKey('userStorageTest');
        $this->bot->shouldReceive('userStorage')
                  ->andReturns($this->storage);

        $this->auth = new Authentication();

        $this->msg = \Mockery::mock(IncomingMessage::class)->makePartial();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->storage->delete();
    }

    public function getDataSet()
    {
        return new YamlDataSet(__DIR__.'/../datas/authentication_datas.yaml');
    }

    public function testCanAuthenticateAValidUser()
    {
        $param['login']    = 'user1';
        $param['password'] = 'user1pass';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_AUTH_SUCCESS);
        $this->authSucess();

        $param['login']    = 'user2';
        $param['password'] = 'user2pass';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_AUTH_SUCCESS);
        $this->authSucess();
    }

    public function testCanNotAuthenticateInvalidCredentials()
    {
        // invalid login
        $param['login']    = 'userwhichdoesntexist';
        $param['password'] = 'user1pass';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_AUTH_FAILURE);
        $this->authFailure();

        // invalid password
        $param['login']    = 'user1';
        $param['password'] = 'invalidpassword';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_AUTH_FAILURE);
        $this->authFailure();
    }

    public function testRepliesCorrectlyToEmptyAuthInfos()
    {
        $param['login']    = '';
        $param['password'] = '';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_EMPTY_INFO);
        $this->authFailure();

        $this->setMessageParameters([]);
        $this->expectToReply(Authentication::MSG_EMPTY_INFO);
        $this->authFailure();
    }

    public function testRepliesCorrectlyToEmptyLogin()
    {
        $param['login']    = '';
        $param['password'] = 'password';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_EMPTY_LOGIN);
        $this->authFailure();
        $this->storage->delete();

        unset($param['login']);
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_EMPTY_LOGIN);
        $this->authFailure();
    }

    public function testRepliesCorrectlyToEmptyPassword()
    {
        $param['login']    = 'user';
        $param['password'] = '';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_EMPTY_PASSWD);
        $this->authFailure();
        $this->storage->delete();

        unset($param['password']);
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_EMPTY_PASSWD);
        $this->authFailure();
    }

    public function testCanMakeTwoStepAuthentication()
    {
        $param['login'] = 'user1';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_EMPTY_PASSWD);
        $this->authFailure();

        unset($param['login']);
        $param['password'] = 'user1pass';
        $this->setMessageParameters($param);
        $this->expectToReply(Authentication::MSG_AUTH_SUCCESS);
        $this->authSucess();
    }

    private function expectToReply($msg)
    {
        $this->bot->shouldReceive('reply')
                  ->once()
                  ->with($msg);
    }

    private function setMessageParameters($parameters)
    {
        $this->msg->shouldReceive('getExtras')
                  ->once()
                  ->with('apiParameters')
                  ->andReturns($parameters);
        $this->bot->shouldReceive('getMessage')
                  ->once()
                  ->andReturns($this->msg);
    }

    private function authenticatedInStorage()
    {
        $flag = $this->storage->get('authenticated');
        $this->assertTrue((bool) $flag);
    }

    private function notAuthenticatedInStorage()
    {
        $flag = $this->storage->get('authenticated');
        $this->assertFalse((bool) $flag);
    }

    private function authSucess()
    {
        $this->assertTrue($this->auth->main($this->bot));
        $this->authenticatedInStorage();
    }

    private function authFailure()
    {
        $this->assertFalse($this->auth->main($this->bot));
        $this->notAuthenticatedInStorage();
    }
}
