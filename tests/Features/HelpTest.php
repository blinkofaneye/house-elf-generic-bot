<?php
/**
 * Tests for the Help class
 */

namespace HouseElf\Tests\Features;

use PHPUnit\Framework\TestCase;

use BotMan\BotMan\BotMan;

use HouseElf\Features\Help;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Help feature
 */
class HelpTest extends TestCase
{
    use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testGetsHelpTextFromConfig()
    {
        $config = ['help_text' => 'Help text!'];
        $mock = $this->loadMock($config['help_text']);
        $feature = $this->loadFeature($config);
        $feature->main($mock);
    }

    public function testRepliesCorrectlyIfHelpTextIsNotFound()
    {
        $mock = $this->loadMock(Help::MSG_HELP_NOT_FOUND);
        $feature = $this->loadFeature([]);
        $feature->main($mock);
    }

    private function loadFeature($config)
    {
        $feature = \Mockery::mock(Help::class)->makePartial();
        $feature->shouldReceive('getConfig')
                ->once()
                ->andReturns($config);
        return $feature;
    }

    private function loadMock($reply)
    {
        $mock = \Mockery::mock(BotMan::class)->makePartial();
        $mock->shouldReceive('reply')
             ->once()
             ->with($reply);
        return $mock;
    }
}
