<?php
/**
 * Tests for the BotConfig class
 */

namespace HouseElf\Tests;

use PHPUnit\Framework\TestCase;

use BotMan\Drivers\Facebook\FacebookDriver;

use HouseElf\Core\BotConfig;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Bot configuration file
 */
class BotConfigTest extends TestCase
{
    private $expectedConfig;

    public function setUp()
    {
        $this->expectedConfig = ['facebook' => ['token'        => 'your token',
                                                'app_secret'   => 'your app secret',
                                                'verification' => 'your verification token'],
                                 'features' => ['authentication_feature' => false,
                                                'greetings_feature'      => true],
                                 'implements_fallback_feature' => false,
                                 'driver' => FacebookDriver::class];
    }

    public function testCanParseBotConfig()
    {
        $config['path'] = __DIR__.'/../datas/test_bot';
        $bot = new BotConfig($config);
        $this->expectedConfig['path'] = __DIR__.'/../datas/test_bot';
        $this->expectedConfig['name'] = 'TestBot';
        $this->expectedConfig['features']['trolls_feature'] = false;
        $this->assertEquals($this->expectedConfig, $bot->getConfig(), 'test_bot has no services but failed');

        $config['path'] = __DIR__.'/../datas/test_bot2';
        $bot = new BotConfig($config);
        $this->expectedConfig['path'] = __DIR__.'/../datas/test_bot2';
        $this->expectedConfig['name']     = 'TestBot2';
        $this->expectedConfig['features']['authentication_feature'] = true;
        $this->expectedConfig['services'] = ['schedule_service'];
        $this->assertEquals($this->expectedConfig, $bot->getConfig(), 'test_bot2 services but failed');
    }

    public function testCanUseConfigWithoutPath()
    {
        $config = ['name' => 'TestName',
                   'facebook' => ['token'        => 'your token',
                                  'app_secret'   => 'your app secret',
                                  'verification' => 'your verification token']];
        $this->expectedConfig['name'] = 'TestName';
        $bot = new BotConfig($config);
        $this->assertEquals($this->expectedConfig, $bot->getConfig());
    }
}
