<?php
/**
 * Tests for the ServiceConfig class
 */

namespace HouseElf\Tests;

use PHPUnit\Framework\TestCase;

use HouseElf\Core\ServiceConfig;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Service configuration file
 */
class ServiceConfigTest extends TestCase
{
    public function testCanParseServiceConfig()
    {
        $config['path'] = __DIR__.'/../datas/test_bot3/services/schedule';
        $bot = new ServiceConfig($config);
        $expectedConfig['path'] = __DIR__.'/../datas/test_bot3/services/schedule';
        $expectedConfig['need_authentication'] = true;
        $this->assertEquals($expectedConfig, $bot->getConfig());

        $config['path'] = __DIR__.'/../datas/test_bot3/services/no_auth';
        $bot = new ServiceConfig($config);
        $expectedConfig['path'] = __DIR__.'/../datas/test_bot3/services/no_auth';
        $expectedConfig['need_authentication'] = false;
        $this->assertEquals($expectedConfig, $bot->getConfig());
    }
}
