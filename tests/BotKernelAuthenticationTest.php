<?php
/**
 * Tests for the integration of authentication in BotKernel
 */

namespace HouseElf\Tests;

use PHPUnit\DbUnit\DataSet\YamlDataSet;

use BotMan\BotMan\Storages\Storage;
use BotMan\BotMan\Storages\Drivers\FileStorage;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;

use HouseElf\BotKernel;
use HouseElf\Features\Authentication;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox  Authentication in BotKernel
 */
class BotKernelAuthenticationTest extends GenericDatabaseTestCase
{
    use BotKernelTrait;

    private $config;
    private $storage;

    public function getDataSet()
    {
        return new YamlDataSet(__DIR__.'/datas/authentication_datas.yaml');
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->config['path'] = __DIR__.'/datas/test_bot5';
        $this->config['database'] = $this->getDBConfig();

        $file_storage = new FileStorage(__DIR__.'/../datas/botman_storage');
        $this->storage = (new Storage($file_storage))->setPrefix('user_')
                                                     ->setDefaultKey('userStorageTest');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->storage->delete();
    }

    public function testCanNotExecuteServiceWhichNeedsAuthenticationIfNotAuthenticated()
    {
        $mock = $this->createReplyMock(BotKernel::MSG_AUTH_NEEDED);
        $bot = $this->createBotMock('test_service', $this->config, [], $mock);
        $bot->listen();
    }

    public function testCanExecuteServiceWhichNeedsAuthenticationIfAuthenticated()
    {
        $mock = $this->createReplyMock(Authentication::MSG_AUTH_SUCCESS);
        $param['login']    = 'user1';
        $param['password'] = 'user1pass';
        $this->setParameters($param, $mock);
        $bot = $this->createBotMock('authentication_feature', $this->config, $param, $mock);
        $bot->listen();

        $mock = $this->createReplyMock('authentication test passed!');
        $bot = $this->createBotMock('test_service', $this->config, [], $mock);
        $bot->listen();
    }

    public function testCanExecuteServiceWhichDoesNotNeedAuthentication()
    {
        $this->config['path'] = __DIR__.'/datas/test_bot6';
        $mock = $this->createReplyMock('authentication test passed!');
        $bot = $this->createBotMock('test_service', $this->config, [], $mock);
        $bot->listen();
    }

    private function createReplyMock(string $reply)
    {
        $mock = BotKernelTrait::createReplyMock($reply);
        $mock->shouldReceive('userStorage')
             ->andReturns($this->storage);
        return $mock;
    }

    private function setParameters($param, &$reply_mock)
    {
        $msg = \Mockery::mock(IncomingMessage::class)->makePartial();
        $msg->shouldReceive('getExtras')
            ->with('apiParameters')
            ->andReturns($param);
        $reply_mock->shouldReceive('getMessage')
                   ->andReturns($msg);
    }
}
