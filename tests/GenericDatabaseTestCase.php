<?php
/**
 * Generic database testcase
 */

namespace HouseElf\Tests;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use Illuminate\Database\Capsule\Manager as Capsule;

use HouseElf\BotKernel;
use HouseElf\Migrations\Botusers;

abstract class GenericDatabaseTestCase extends TestCase
{
    use TestCaseTrait;

    /** @var PHPUnit\DbUnit\Database\Connection */
    private $conn = null;

    /** @var array */
    private $db;

    public function __construct()
    {
        parent::__construct();

        $this->db = [
            'driver'                  => 'sqlite',
            'database'                => __DIR__.'/datas/db.sqlite',
            'prefix'                  => '',
            'foreign_key_constraints' => true,
        ];

        // set up database
        file_put_contents($this->db['database'], '');
        BotKernel::setUpDatabase($this->db);
        (new Botusers)->up();
    }

    public function __destruct()
    {
        @unlink($this->db['database']);
    }

    final public function getConnection()
    {
        if ($this->conn === null) {
            $this->conn = $this->createDefaultDBConnection(
                Capsule::connection()->getPdo(),
                $this->db['database'],
            );
        }
        return $this->conn;
    }

    final public function getDBConfig()
    {
        return $this->db;
    }
}
