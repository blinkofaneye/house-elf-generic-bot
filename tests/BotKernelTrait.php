<?php
/**
 * BotKernel trait
 */

namespace HouseElf\Tests;

use PHPUnit\Framework\TestCase;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Middleware\Dialogflow;

use HouseElf\BotKernel;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Bot kernel
 */
trait BotKernelTrait
{
    use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function createDialogflowMock($action, $param = [], $reply = '')
    {
        $dialogflow = \Mockery::mock(Dialogflow::class)->makePartial();
        $dialogflow->shouldReceive('received')
                   ->andReturnUsing(function ($message, $next, $bot) use ($action, $param, $reply) {
                        $message->addExtras('apiReply', $reply);
                        $message->addExtras('apiAction', $action);
                        $message->addExtras('apiActionIncomplete', false);
                        $message->addExtras('apiIntent', 'intent');
                        $message->addExtras('apiParameters', $param);

                        return $next($message);
                   });

        return $dialogflow;
    }

    public function createBotMock($action, $config = null, $param = [], $mock = null)
    {
        if ($config === null) {
            $config['features'] = ['test_feature' => true];
            $config['services'] = ['test_service'];
        }

        $dialogflow = $this->createDialogflowMock($action, $param);
        $bot = \Mockery::mock(BotKernel::class, [$config, $dialogflow, null, $mock])->makePartial();
        $bot->shouldAllowMockingProtectedMethods();
        return $bot;
    }

    public function createReplyMock(string $reply)
    {
        $mock = \Mockery::mock(BotMan::class)->makePartial();
        $mock->shouldReceive('reply')
             ->once()
             ->with($reply);
        return $mock;
    }
}
