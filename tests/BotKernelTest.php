<?php
/**
 * Tests for the BotKernel class
 */

namespace HouseElf\Tests;

use PHPUnit\Framework\TestCase;

use BotMan\BotMan\BotMan;
use BotMan\Drivers\Facebook\FacebookDriver;
use BotMan\BotMan\Drivers\DriverManager;

use Mockery as m;

use HouseElf\BotKernel;
use HouseElf\Features\AbstractFeature;

/**
 * Test case
 *
 * @package generic_bot
 * @category test
 * @author blinkofaneye
 * @testdox Bot kernel
 */
class BotKernelTest extends TestCase
{
    use BotKernelTrait;

    public function testExecutesActivatedFeature()
    {
        $bot = $this->createBotMock('test_feature');

        $bot->shouldReceive('executeFeature')
            ->once()
            ->with('test_feature', \Mockery::any());

        $bot->listen();
    }

    public function testDoesNotRegisterDeactivatedFeature()
    {
        $bot = $this->createBotMock('test_feature');

        $bot->shouldNotReceive('registerFeature')
            ->with('test_feature', \Mockery::any());

        $bot->shouldReceive('executeFeature')
            ->once()
            ->with('fallback_feature', \Mockery::any());

        $bot->deactivateFeature('test_feature');
        $bot->listen();
    }

   /**
    * @expectedException Symfony\Component\Yaml\Exception\ParseException
    * @expectedExceptionMessage File "/config.yaml" does not exist.
    */
    public function testExecutesService()
    {
        $bot = $this->createBotMock('test_service', ['path' => '']);

        $bot->shouldReceive('executeService')
            ->once()
            ->with('test_service', \Mockery::any());

        $bot->listen();
    }

    public function testExecutesFallbackFeatureIfThereIsNoMatch()
    {
        $bot = $this->createBotMock('no_match_action');

        $bot->shouldReceive('executeFeature')
            ->once()
            ->with('fallback_feature', \Mockery::any());

        $bot->listen();
    }

    public function testCanInstantiateAClassAndExecuteOneOfItsMethod()
    {
        $bar_class = '\\HouseElf\\Tests\\Bar';
        $foo_class = '\\HouseElf\\Tests\\Foo';
        $foo = \Mockery::mock("overload:{$foo_class}", $bar_class);
        $foo->shouldReceive('method')
            ->once()
            ->with(1, 2)
            ->andReturns(true);

        $this->assertTrue(BotKernel::instantiateAndExecute(
            $foo_class,
            $bar_class,
            'method',
            1,
            2
        ));
    }

    public function testCanExecuteARealFeature()
    {
        require_once __DIR__.'/Features/TestFeature.php';
        $dialogflow = $this->createDialogflowMock('test_feature');
        $mock = $this->createReplyMock('test feature executed!');
        $bot = new BotKernel(
            ['features' => ['test_feature' => true]],
            $dialogflow,
            null,
            $mock
        );
        $bot->listen();
    }

    public function testCanExecuteARealService()
    {
        $config['path'] = __DIR__.'/datas/test_bot4';
        $dialogflow = $this->createDialogflowMock('test_service');
        $mock = $this->createReplyMock('test service executed!');
        $bot = new BotKernel(
            $config,
            $dialogflow,
            null,
            $mock
        );
        $bot->listen();
    }

    public function testCanExecuteARealFallbackFeature()
    {
        $config['path'] = __DIR__.'/datas/test_bot4';
        $dialogflow = $this->createDialogflowMock('no_match_action');
        $mock = $this->createReplyMock('fallback feature executed!');
        $bot = new BotKernel(
            $config,
            $dialogflow,
            null,
            $mock
        );
        $bot->listen();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testLoadsCorrectDriver()
    {
        $driverManager = m::mock('overload:'.DriverManager::class);
        $driverManager->shouldReceive('loadDriver')
                      ->once()
                      ->with(FacebookDriver::class);
        $bot = m::mock(BotKernel::class)->makePartial();
        $bot->loadConfig([]);
        $bot->loadDriver();
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessageRegExp /^Unknown driver Invalid driver$/
     */
    public function testThrowsExceptionOnUnknownDriver()
    {
        $bot = m::mock(BotKernel::class)->makePartial();
        $bot->loadConfig(['driver' => 'Invalid driver']);
        $bot->loadDriver();
    }
}
