#!/usr/bin/env php

<?php

if (file_exists(__DIR__.'/../vendor/autoload.php')) {
    require_once __DIR__.'/../vendor/autoload.php';
} else {
    require_once realpath(dirname($argv[0]).'/../autoload.php');
}

use Symfony\Component\Console\Application;

use HouseElf\Commands\Migrate;

$app = new Application();

$app->add(new Migrate);

$app->run();
